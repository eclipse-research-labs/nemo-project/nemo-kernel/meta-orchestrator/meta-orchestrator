package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/url"
	"os"
	"os/exec"
	"strings"

	"github.com/streadway/amqp"
	"gopkg.in/yaml.v2"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	"k8s.io/kubectl/pkg/scheme"
	workclient "open-cluster-management.io/api/client/work/clientset/versioned"
	workv1 "open-cluster-management.io/api/work/v1"
)

// Define the YAML data structure
type Manifestwork struct {
	APIVersion string `yaml:"apiVersion"`
	Kind       string `yaml:"kind"`
	Metadata   struct {
		Name      string `yaml:"name"`
		Namespace string `yaml:"namespace"`
	} `yaml:"metadata"`
	Spec struct {
		Workload struct {
			Manifests []interface{} `yaml:"manifests"`
		} `yaml:"workload"`
	} `yaml:"spec"`
}

func main() {

	var manifestwork Manifestwork
	manifestWorkName := os.Getenv("MANIFESTWORK")
	namespace := os.Getenv("DEPLOYMENT_CLUSTER")
	manifestwork.Metadata.Name = manifestWorkName
	manifestwork.Metadata.Namespace = namespace

	// Create in-cluster config
	config, err := rest.InClusterConfig()
	// If running outside the cluster creates config from kubeconfig file
	if err != nil {
		log.Println("Unable to create in-cluster config:", err)
		config = SetupConfig()
	}
	clientsetWorkOper, err := workclient.NewForConfig(config)
	if err != nil {
		fmt.Println("Error creating clientset:", err)
		return
	}

	// RabbitMQ connection URL
	username := os.Getenv("RABBITMQ_USERNAME")
	password := os.Getenv("RABBITMQ_PASSWORD")
	rabbitMQServerIP := os.Getenv("RABBITMQ_SERVER_IP")
	rabbitMQPort := os.Getenv("RABBITMQ_PORT")
	connURL := fmt.Sprintf("amqp://%s:%s@%s:%s/", url.QueryEscape(username), url.QueryEscape(password), rabbitMQServerIP, rabbitMQPort)

	// Connect to RabbitMQ
	conn, err := amqp.Dial(connURL)
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %v", err)
	}
	defer conn.Close()

	// Create a channel
	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %v", err)
	}
	defer ch.Close()

	// Consume messages from the queue
	msgs, err := ch.Consume(
		"mo-deploy", // queue
		"",          // consumer
		true,        // auto-ack
		false,       // exclusive
		false,       // no-local
		false,       // no-wait
		nil,         // args
	)
	if err != nil {
		log.Fatalf("Failed to register a consumer: %v", err)
	}

	fmt.Println("Awaiting message...")

	// Infinite loop to continuously read messages
	for msg := range msgs {
		//fmt.Printf("Received a message:\n %s\n", msg.Body)

		// Unmarshal the JSON data into a map
		var data map[string]interface{}
		err = json.Unmarshal(msg.Body, &data)
		if err != nil {
			fmt.Println("Error:", err)
			return
		}

		// Extract the manifests
		manifests, ok := data["manifests"].([]interface{})
		if !ok {
			fmt.Println("Error: 'manifests' field is not an array")
			return
		}

		// Append the manifests to the YAML structure
		for _, manifest := range manifests {
			manifestMap, ok := manifest.(map[string]interface{})
			if !ok {
				fmt.Println("Error: Manifest is not a map")
				continue
			}
			// If namespace field is missing, assign "default" value
			if _, exists := manifestMap["metadata"].(map[string]interface{})["namespace"]; !exists {
				manifestMap["metadata"].(map[string]interface{})["namespace"] = "default"
			}
			manifestwork.Spec.Workload.Manifests = append(manifestwork.Spec.Workload.Manifests, manifestMap)
		}

		// Marshal the YAML data
		yamlBytes, err := yaml.Marshal(&manifestwork)
		if err != nil {
			fmt.Println("Error marshaling YAML:", err)
			return
		}

		// Prepare ManifestWork data
		decoder := serializer.NewCodecFactory(scheme.Scheme).UniversalDecoder()
		manifestWork := &workv1.ManifestWork{}
		err = runtime.DecodeInto(decoder, []byte(yamlBytes), manifestWork)
		if err != nil {
			fmt.Println("Error decoding manifestwork:", err)
			return
		}
		_, err = clientsetWorkOper.WorkV1().
			ManifestWorks(namespace).Create(context.TODO(),
			manifestWork, metav1.CreateOptions{})

		if err != nil {
			fmt.Println("Error applying manifestwork:", err)
			return
		}

		// Print result
		fmt.Println("\nManifestWork applied successfully")
		fmt.Println("\nAwaiting message...")
	}
}

func SetupConfig() *rest.Config {
	// Get kubernetes version
	var kubeconfig *string
	cmd := exec.Command("kubectl", "version", "--client=true")
	// Run the command and capture the output
	output, err := cmd.Output()
	if err != nil {
		log.Fatal(err)
	}
	// Check if version is k3s or k8s and assign kubeconfig file accordingly
	if strings.Contains(string(output), "k3s") {
		kubeconfig = flag.String("kubeconfig", "/etc/rancher/k3s/k3s.yaml", "absolute path to the kubeconfig file")
	} else {
		home := homedir.HomeDir()
		kubeconfig = flag.String("kubeconfig", home+"/.kube/config", "absolute path to the kubeconfig file")
	}
	// Use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		fmt.Printf("Error building kubeconfig: %v\n", err)
		os.Exit(1)
	}
	return config
}
