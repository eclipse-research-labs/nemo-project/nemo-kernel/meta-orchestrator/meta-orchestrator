/*******************************************************************************
 * Copyright 2023 Bull SAS
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package main

import (
	"fmt"
	"log"
	"net/url"
	"os"

	"github.com/streadway/amqp"
)

func main() {

	// Read the input yaml
	data, err := os.ReadFile("./manifests/workload.json")
	if err != nil {
		panic(err)
	}

	// RabbitMQ connection URL
	username := os.Getenv("RABBITMQ_USERNAME")
	password := os.Getenv("RABBITMQ_PASSWORD")
	rabbitMQServerIP := os.Getenv("RABBITMQ_SERVER_IP")
	rabbitMQPort := os.Getenv("RABBITMQ_PORT")
	connURL := fmt.Sprintf("amqp://%s:%s@%s:%s/", url.QueryEscape(username), url.QueryEscape(password), rabbitMQServerIP, rabbitMQPort)

	// Create a new RabbitMQ connection
	connectRabbitMQ, err := amqp.Dial(connURL)
	if err != nil {
		panic(err)
	}

	// Open a channel
	channelRabbitMQ, err := connectRabbitMQ.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %v", err)
	}
	defer channelRabbitMQ.Close()

	// Publish a message
	err = channelRabbitMQ.Publish(
		"ibmc",                   // exchange
		"deploy-svc-nodes-reply", // routing key
		false,                    // mandatory
		false,                    // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(data),
		},
	)
	if err != nil {
		log.Fatalf("Failed to trigger the deployment: %v", err)
	}
	fmt.Println("Deployment triggered")
}
