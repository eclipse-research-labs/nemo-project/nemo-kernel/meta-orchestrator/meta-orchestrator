/*******************************************************************************
 * Copyright 2023 Bull SAS
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package k8s

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
)

var kubeconfig *string

func SetupConfig() (*kubernetes.Clientset, *rest.Config) {

	config, err := rest.InClusterConfig()
    if err != nil {
        // If the in-cluster config fails, fall back to kubeconfig
        kubeconfig := filepath.Join(homedir.HomeDir(), ".kube", "config")
        if kubeconfigPath := os.Getenv("KUBECONFIG"); kubeconfigPath != "" {
            kubeconfig = kubeconfigPath
        }
        config, err = clientcmd.BuildConfigFromFlags("", kubeconfig)
        if err != nil {
            log.Fatalf("Failed to build kubeconfig: %v", err)
        }
    }
	// Create the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		fmt.Printf("Error creating clientset: %v\n", err)
		os.Exit(1)
	}
	return clientset, config
}
