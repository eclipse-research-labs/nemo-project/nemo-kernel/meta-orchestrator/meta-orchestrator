{
    "swagger": "2.0",
    "info": {
        "description": "A MetaOrchestrator service API in Go using Gin framework",
        "title": "MetaOrchestrator API",
        "contact": {},
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "version": "1.0.4-beta"
    },
    "host": "mo.nemo.onelab.eu",
    "basePath": "/",
    "paths": {
        "/addClusterArgo": {
            "put": {
                "security": [
                    {
                        "BearerAuth": []
                    }
                ],
                "description": "Using the ArgoCD API, the HUB cluster is adding an existing cluster inside OCM to the ArgoCD list of clusters.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "workloads (Intent-API)"
                ],
                "summary": "Add an OCM Cluster to ArgoCD",
                "parameters": [
                    {
                        "description": "ArgoCD Token",
                        "name": "argocd_token",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "ok",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/deployWorkload": {
            "post": {
                "security": [
                    {
                        "BearerAuth": []
                    }
                ],
                "description": "Integration with Intent Based API to deploy a workload",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "workloads (Intent-API)"
                ],
                "summary": "Deploy a workload",
                "parameters": [
                    {
                        "description": "Helm Chart",
                        "name": "helm_chart",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "ok",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/getHubToken": {
            "get": {
                "security": [
                    {
                        "BearerAuth": []
                    }
                ],
                "description": "Retrieves the HUB token with OCM context.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "ocm"
                ],
                "summary": "Get HUB token with OCM context",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    }
                }
            }
        },
        "/getInfoHub": {
            "get": {
                "security": [
                    {
                        "BearerAuth": []
                    }
                ],
                "description": "Get info from the HUB about network, OCM, interfaces",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "ocm"
                ],
                "summary": "Get info from the HUB cluster",
                "parameters": [
                    {
                        "description": "Cluster Name",
                        "name": "cluster_name",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "ok",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/getInfoManagedCluster": {
            "get": {
                "security": [
                    {
                        "BearerAuth": []
                    }
                ],
                "description": "Get info from managed clusters",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "ocm"
                ],
                "summary": "Get info about Managed Clusters",
                "responses": {
                    "200": {
                        "description": "Managed Clusters details",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.ClusterDetails"
                            }
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    }
                }
            }
        },
        "/joinManagedCluster": {
            "put": {
                "security": [
                    {
                        "BearerAuth": []
                    }
                ],
                "description": "Join a managed cluster into the OCM network.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "ocm"
                ],
                "summary": "Join a managed cluster",
                "parameters": [
                    {
                        "description": "Info that hub needs to join a cluster",
                        "name": "info",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/controller.JoinRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    }
                }
            }
        },
        "/login": {
            "post": {
                "description": "Authenticate a user and generate a JWT token",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "auth"
                ],
                "summary": "User login",
                "parameters": [
                    {
                        "description": "Login credentials",
                        "name": "credentials",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/controller.LoginCredentials"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    }
                }
            }
        },
        "/unjoinManagedCluster": {
            "delete": {
                "security": [
                    {
                        "BearerAuth": []
                    }
                ],
                "description": "Delete a managed cluster from the OCM network.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "ocm"
                ],
                "summary": "Unjoin a managed cluster",
                "parameters": [
                    {
                        "description": "Info that hub needs to unjoin a cluster",
                        "name": "info",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/controller.JoinRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    },
                    "401": {
                        "description": "Unauthorized",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/controller.Response"
                        }
                    }
                }
            }
        },
        "/updateWorkloadStatus": {
            "put": {
                "security": [
                    {
                        "BearerAuth": []
                    }
                ],
                "description": "This request will update the status of the workload for the Intent API",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "workloads (Intent-API)"
                ],
                "summary": "Update a workload status",
                "parameters": [
                    {
                        "description": "Helm Chart",
                        "name": "helm_chart",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "ok",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "controller.JoinRequest": {
            "type": "object",
            "required": [
                "cluster_name",
                "hub_token",
                "managed_api"
            ],
            "properties": {
                "cluster_name": {
                    "type": "string"
                },
                "hub_token": {
                    "type": "string"
                },
                "managed_api": {
                    "type": "string"
                }
            }
        },
        "controller.LoginCredentials": {
            "type": "object",
            "properties": {
                "password": {
                    "type": "string"
                },
                "username": {
                    "type": "string"
                }
            }
        },
        "controller.Response": {
            "type": "object",
            "properties": {
                "response": {
                    "type": "string"
                },
                "status": {
                    "type": "string"
                }
            }
        },
        "models.ClusterClaim": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "value": {
                    "type": "string"
                }
            }
        },
        "models.ClusterDetails": {
            "type": "object",
            "properties": {
                "allocatable": {
                    "type": "object",
                    "additionalProperties": {
                        "type": "string"
                    }
                },
                "annotations": {
                    "type": "object",
                    "additionalProperties": {
                        "type": "string"
                    }
                },
                "capacity": {
                    "type": "object",
                    "additionalProperties": {
                        "type": "string"
                    }
                },
                "clusterClaims": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/models.ClusterClaim"
                    }
                },
                "conditions": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/models.Condition"
                    }
                },
                "creationTimestamp": {
                    "type": "string"
                },
                "hubAcceptsClient": {
                    "type": "boolean"
                },
                "labels": {
                    "type": "object",
                    "additionalProperties": {
                        "type": "string"
                    }
                },
                "leaseDurationSeconds": {
                    "type": "integer"
                },
                "managedClusterClientConfigs": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/models.ManagedClusterClientConfig"
                    }
                },
                "name": {
                    "type": "string"
                },
                "uid": {
                    "type": "string"
                },
                "version": {
                    "type": "string"
                }
            }
        },
        "models.Condition": {
            "type": "object",
            "properties": {
                "lastTransitionTime": {
                    "type": "string"
                },
                "message": {
                    "type": "string"
                },
                "reason": {
                    "type": "string"
                },
                "status": {
                    "type": "string"
                },
                "type": {
                    "type": "string"
                }
            }
        },
        "models.ManagedClusterClientConfig": {
            "type": "object",
            "properties": {
                "caBundle": {
                    "type": "string"
                },
                "url": {
                    "type": "string"
                }
            }
        }
    },
    "securityDefinitions": {
        "BearerAuth": {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header"
        }
    }
}