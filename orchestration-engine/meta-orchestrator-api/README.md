
# MetaOrchestrator API

MetaOrchestrator is an API service built in Go using the Gin framework. This API is designed to orchestrate operation about OCM, workloads, argocd inside the NEMO project.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Documentation](#documentation)
- [License](#license)
- [Contributing](#contributing)
- [Credits](#credits)

## Installation

### Prerequisites

- Go 1.16 or higher
- Git
- [Gin](https://github.com/gin-gonic/gin)
- [Zerolog](https://github.com/rs/zerolog)
- [Godotenv](https://github.com/joho/godotenv)


### Configure environment variables

Create a `.env` file in the project root and define the necessary variables.

```bash
touch .env
```

Example content of the `.env` file:

```plaintext
# .env
APP_USERNAME='xxxxxx'
APP_PASSWORD_HASH='xxxxxxxxxxxx'
SECRET_KEY='xxxxxxxxxxxxx'
API_PORT='xxxx'
```

### Install dependencies

```bash
go mod tidy
```

## Usage

### Run the server

```bash
go run main.go
```

The server will start at `https://mo.nemo.onelab.eu/`.

### Available Endpoints

You can access the full documentation of the endpoints at `https://mo.nemo.onelab.eu/docs/index.html`.

## Documentation

This API follows the OpenAPI specification. Documentation is available and can be automatically generated using tools like Swagger.

### Generate documentation

If you make changes to the endpoints, make sure to update the documentation:

```bash
swag init
```

## License

This project is licensed under the Apache 2.0 License. See the [LICENSE](LICENSE) file for details.

## Contributing

Contributions are welcome! Please follow these steps to contribute:

1. Fork the project.
2. Create a new branch (`git checkout -b feature/new-feature`).
3. Make your changes and commit them (`git commit -am 'Add new feature'`).
4. Push your branch to the main repository (`git push origin feature/new-feature`).
5. Open a Pull Request.

## Credits

- [Gin](https://github.com/gin-gonic/gin) - Web framework
- [Zerolog](https://github.com/rs/zerolog) - Logging library
- [Godotenv](https://github.com/joho/godotenv) - Environment variable loader