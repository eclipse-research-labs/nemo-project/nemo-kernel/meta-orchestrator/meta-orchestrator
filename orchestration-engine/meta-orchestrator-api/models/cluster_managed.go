/*******************************************************************************
 * Copyright 2023 Bull SAS
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/
package models

import (
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    ocmv1 "open-cluster-management.io/api/cluster/v1"
)

type ClusterDetails struct {
    Name                       string            `json:"name"`
    UID                        string            `json:"uid"`
    CreationTimestamp          metav1.Time       `json:"creationTimestamp"`
    Labels                     map[string]string `json:"labels"`
    Annotations                map[string]string `json:"annotations"`
    ManagedClusterClientConfigs []ManagedClusterClientConfig `json:"managedClusterClientConfigs"`
    HubAcceptsClient           bool              `json:"hubAcceptsClient"`
    LeaseDurationSeconds       int32             `json:"leaseDurationSeconds"`
    Conditions                 []Condition       `json:"conditions"`
    ClusterClaims              []ClusterClaim    `json:"clusterClaims"`
    Capacity                   map[string]string `json:"capacity"`
    Allocatable                map[string]string `json:"allocatable"`
    Version                    string            `json:"version"`
}

type ManagedClusterClientConfig struct {
    URL      string `json:"url"`
    CABundle string `json:"caBundle,omitempty"`
}

type Condition struct {
    Type               string      `json:"type"`
    Status             string      `json:"status"`
    Reason             string      `json:"reason"`
    Message            string      `json:"message"`
    LastTransitionTime metav1.Time `json:"lastTransitionTime"`
}

type ClusterClaim struct {
    Name  string `json:"name"`
    Value string `json:"value"`
}

func GetClusterDetails(cluster ocmv1.ManagedCluster) ClusterDetails {
    details := ClusterDetails{
        Name:                       cluster.Name,
        UID:                        string(cluster.UID),
        CreationTimestamp:          cluster.CreationTimestamp,
        Labels:                     cluster.Labels,
        Annotations:                cluster.Annotations,
        ManagedClusterClientConfigs: []ManagedClusterClientConfig{},
        HubAcceptsClient:           cluster.Spec.HubAcceptsClient,
        LeaseDurationSeconds:       cluster.Spec.LeaseDurationSeconds,
        Conditions:                 []Condition{},
        ClusterClaims:              []ClusterClaim{},
        Capacity:                   map[string]string{},
        Allocatable:                map[string]string{},
        Version:                    cluster.Status.Version.Kubernetes,
    }

    for _, config := range cluster.Spec.ManagedClusterClientConfigs {
        mcConfig := ManagedClusterClientConfig{
            URL: config.URL,
        }
        if config.CABundle != nil {
            mcConfig.CABundle = string(config.CABundle)
        }
        details.ManagedClusterClientConfigs = append(details.ManagedClusterClientConfigs, mcConfig)
    }

    for _, condition := range cluster.Status.Conditions {
        details.Conditions = append(details.Conditions, Condition{
            Type:               condition.Type,
            Status:             string(condition.Status), 
            Reason:             condition.Reason,
            Message:            condition.Message,
            LastTransitionTime: condition.LastTransitionTime,
        })
    }

    for _, claim := range cluster.Status.ClusterClaims {
        details.ClusterClaims = append(details.ClusterClaims, ClusterClaim{
            Name:  claim.Name,
            Value: claim.Value,
        })
    }

    for resource, quantity := range cluster.Status.Capacity {
        details.Capacity[string(resource)] = quantity.String()
    }

    for resource, quantity := range cluster.Status.Allocatable {
        details.Allocatable[string(resource)] = quantity.String()
    }

    return details
}
