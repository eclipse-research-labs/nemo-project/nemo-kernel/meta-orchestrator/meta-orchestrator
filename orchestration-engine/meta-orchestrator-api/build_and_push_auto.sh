#!/bin/bash

# Set default values for image name and namespace
IMAGE_NAME=${IMAGE_NAME:-"mo_api"}
NAMESPACE=${NAMESPACE:-"rubens90"}
TAG_SUFFIX=$(date +%Y%m%d%H%M%S) # Use timestamp as tag suffix
FULL_IMAGE_NAME="${IMAGE_NAME}:${TAG_SUFFIX}"
REMOTE_IMAGE_NAME="${NAMESPACE}/${FULL_IMAGE_NAME}"

# Build the Docker image
echo "Building Docker image ${FULL_IMAGE_NAME}..."
docker build -t ${FULL_IMAGE_NAME} .

# Check if the build was successful
if [ $? -ne 0 ]; then
  echo "Error: Docker build failed"
  exit 1
fi

# List Docker images
echo "Listing Docker images..."
docker images

# Tag the Docker image
echo "Tagging Docker image ${FULL_IMAGE_NAME} as ${REMOTE_IMAGE_NAME}..."
docker tag ${FULL_IMAGE_NAME} ${REMOTE_IMAGE_NAME}

# Check if the tagging was successful
if [ $? -ne 0 ]; then
  echo "Error: Docker tag failed"
  exit 1
fi

# Push the Docker image to the remote repository
echo "Pushing Docker image ${REMOTE_IMAGE_NAME}..."
docker push ${REMOTE_IMAGE_NAME}

# Check if the push was successful
if [ $? -ne 0 ]; then
  echo "Error: Docker push failed"
  exit 1
fi

echo "Docker image ${REMOTE_IMAGE_NAME} pushed successfully"
