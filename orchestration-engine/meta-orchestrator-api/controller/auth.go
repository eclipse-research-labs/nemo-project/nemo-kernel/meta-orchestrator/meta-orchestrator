/*******************************************************************************
 * Copyright 2023 Bull SAS
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/
package controller

import (
	"crypto/subtle"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"mo_api/middleware"
)

type LoginCredentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func Login(c *gin.Context) {
	var creds struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	if err := c.ShouldBindJSON(&creds); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"response": err.Error(),
			"status":   "error",
		})
		return
	}

	expectedUsername := os.Getenv("APP_USERNAME")
	encryptedPassword := os.Getenv("APP_PASSWORD_HASH")
	secretKey := os.Getenv("SECRET_KEY")

	expectedPassword, err := middleware.Decrypt(encryptedPassword, secretKey)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"response": "Failed to decrypt password",
			"status":   "error",
		})
		return
	}

	if creds.Username != expectedUsername || subtle.ConstantTimeCompare([]byte(creds.Password), []byte(expectedPassword)) != 1 {
		c.JSON(http.StatusUnauthorized, gin.H{
			"response": "Invalid credentials",
			"status":   "error",
		})
		return
	}

	token, err := middleware.GenerateJWT(creds.Username)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"response": "Failed to generate token",
			"status":   "error",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": "ok", "token": token})
}
