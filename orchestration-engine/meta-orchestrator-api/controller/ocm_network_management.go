/*******************************************************************************
 * Copyright 2023 Bull SAS
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/
package controller

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	authv1 "k8s.io/api/authentication/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/utils/ptr"
	"mo_api/models"
	"mo_api/pkg/k8s"
	"open-cluster-management.io/clusteradm/pkg/config"

	ocmclientset "open-cluster-management.io/api/client/cluster/clientset/versioned"
	ocmv1 "open-cluster-management.io/api/cluster/v1" // Asegúrate de usar el paquete correcto
)

func GetBootstrapTokenFromSA(ctx context.Context, kubeClient kubernetes.Interface) (string, error) {
	tr, err := kubeClient.CoreV1().
		ServiceAccounts(config.OpenClusterManagementNamespace).
		CreateToken(ctx, config.BootstrapSAName, &authv1.TokenRequest{
			Spec: authv1.TokenRequestSpec{
				// token expired in 1 hour
				ExpirationSeconds: ptr.To[int64](3600),
			},
		}, metav1.CreateOptions{})
	if err != nil {
		return "", fmt.Errorf("failed to get token from sa %s/%s: %v", config.OpenClusterManagementNamespace, config.BootstrapSAName, err)
	}
	return tr.Status.Token, nil
}

// ValidateOCMToken valida un token de OCM haciendo una solicitud a la API del hub
// func ValidateOCMToken(ManagedAPI string, token string) error {
// 	config := &rest.Config{
// 		Host:        ManagedAPI,
// 		BearerToken: token,
// 		TLSClientConfig: rest.TLSClientConfig{
// 			Insecure: true, // TODO: check for a real scenario
// 		},
// 	}

// 	ocmClient, err := ocmclientset.NewForConfig(config)
// 	if err != nil {
// 		return err
// 	}

// 	// List the managed clusters for token validation
// 	_, err = ocmClient.ClusterV1().ManagedClusters().List(context.TODO(), metav1.ListOptions{})
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }

func ValidationBodyRequest(c *gin.Context) (error, models.JoinRequest) {
	var request models.JoinRequest

	// Bind JSON al request struct
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusInternalServerError,
			gin.H{
				"status":   "error",
				"response": fmt.Sprintf("Error %v\n", err.Error()),
			})
		return err, request
	}

	// apiServer := os.Getenv("HUB_API")
	// if err := ValidateOCMToken(apiServer, request.HubToken); err != nil {
	// 	c.JSON(http.StatusUnauthorized,
	// 		gin.H{
	// 			"status":   "error",
	// 			"response": "Validation Token: " + err.Error(),
	// 		})
	// 	return err, request
	// }

	return nil, request
}

// JoinCluster handles the request to join a managed cluster to the hub
func JoinManagedCluster(c *gin.Context) {
	_, config := k8s.SetupConfig()
	err, request := ValidationBodyRequest(c)
	if err != nil {
		return
	}

	clusterClient, err := ocmclientset.NewForConfig(config)
	if err != nil {
		c.JSON(http.StatusInternalServerError,
			gin.H{
				"status":   "error",
				"response": fmt.Sprintf("Error creating OCM client: %v", err),
			})
		return
	}

	managedCluster := &ocmv1.ManagedCluster{
		ObjectMeta: metav1.ObjectMeta{
			Name: request.ClusterName,
		},
		Spec: ocmv1.ManagedClusterSpec{
			HubAcceptsClient: true,
			ManagedClusterClientConfigs: []ocmv1.ClientConfig{
				{
					URL: request.ManagedAPI,
				},
			},
		},
	}

	_, err = clusterClient.ClusterV1().ManagedClusters().Create(context.TODO(), managedCluster, metav1.CreateOptions{})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":   "error",
			"response": fmt.Sprintf("Error creating ManagedCluster: %v", err),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": "ok", "response": "ManagedCluster created successfully"})
}

func UnjoinManagedCluster(c *gin.Context) {
	_, config := k8s.SetupConfig()
	err, request := ValidationBodyRequest(c)
	if err != nil {
		return
	}

	// Creates  OCM client
	clusterClient, err := ocmclientset.NewForConfig(config)
	if err != nil {
		c.JSON(http.StatusInternalServerError,
			gin.H{
				"status":   "error",
				"response": fmt.Sprintf("Error creating OCM client: %v", err),
			})
		return
	}
	// Remove a ManagedCluster
	err = clusterClient.ClusterV1().ManagedClusters().Delete(context.TODO(), request.ClusterName, metav1.DeleteOptions{})
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"status":   "error",
			"response": fmt.Sprintf("Error unjoining custer: %v", err),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": "ok", "response": "ManagedCluster unjoined successfully"})
}
