/*******************************************************************************
 * Copyright 2023 Bull SAS
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/
package controller

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"mo_api/models"
	"mo_api/pkg/k8s"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	ocmclientset "open-cluster-management.io/api/client/cluster/clientset/versioned"
	// ocmv1 "open-cluster-management.io/api/cluster/v1"
)

type Controller struct{}

// JoinRequest defines the structure for the join request parameters
type JoinRequest struct {
	HubToken    string `json:"hub_token" binding:"required"`
	ManagedAPI  string `json:"managed_api" binding:"required"`
	ClusterName string `json:"cluster_name" binding:"required"`
}

// Response model
type Response struct {
	Status   string `json:"status"`
	Response string `json:"response"`
}

// Login handles the login request and generates a JWT token
// @Summary User login
// @Description Authenticate a user and generate a JWT token
// @Tags auth
// @Accept json
// @Produce json
// @Param credentials body LoginCredentials true "Login credentials"
// @Success 200 {object} Response
// @Failure 400 {object} Response
// @Failure 401 {object} Response
// @Failure 500 {object} Response
// @Router /login [post]
func (ctr *Controller) Login(c *gin.Context) {
	Login(c)
}

// GetInfoManagedClusters retrieves information about managed clusters
// @Summary Get info about Managed Clusters
// @Security BearerAuth
// @Schemes
// @Description Get info from managed clusters
// @Tags ocm
// @Accept json
// @Produce json
// @Success 200 {object} []models.ClusterDetails "Managed Clusters details"
// @Failure 500 {object} Response
// @Router /getInfoManagedCluster [get]
func (ctr *Controller) GetInfoManagedClusters(c *gin.Context) {
	log.Info().Msg("GetInfoManagedClusters init")
	_, config := k8s.SetupConfig()

	ocmClient, err := ocmclientset.NewForConfig(config)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":   "error",
			"response": fmt.Sprintf("Failed to create OCM client: %v", err),
		})
		return
	}

	managedClusters, err := ocmClient.ClusterV1().ManagedClusters().List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":   "error",
			"response": fmt.Sprintf("Failed to list managed clusters: %v", err),
		})
		return
	}

	var clusterDetailsList []models.ClusterDetails

	for _, cluster := range managedClusters.Items {
		clusterDetailsList = append(clusterDetailsList, models.GetClusterDetails(cluster))
		// printClusterDetails(cluster)
	}

	c.JSON(http.StatusOK, gin.H{
		"status":   "ok",
		"response": clusterDetailsList,
	})
}

// func printClusterDetails(cluster ocmv1.ManagedCluster) {
// 	fmt.Printf("Name: %v\n", cluster)
// 	fmt.Printf("\n")
// }

// JoinManagedCluster allows joining a managed cluster to the OCM network
// @Summary Join a managed cluster
// @Security BearerAuth
// @Schemes
// @Description Join a managed cluster into the OCM network.
// @Tags ocm
// @Accept json
// @Produce json
// @Param info body JoinRequest true "Info that hub needs to join a cluster"
// @Success 200 {object} Response
// @Failure 400 {object} Response
// @Failure 401 {object} Response
// @Failure 500 {object} Response
// @Router /joinManagedCluster [put]
func (ctr *Controller) JoinManagedCluster(c *gin.Context) {
	JoinManagedCluster(c)
}

// UnjoinManagedCluster allows removing a managed cluster from the OCM network
// @Summary Unjoin a managed cluster
// @Security BearerAuth
// @Schemes
// @Description Delete a managed cluster from the OCM network.
// @Tags ocm
// @Accept json
// @Produce json
// @Param info body JoinRequest true "Info that hub needs to unjoin a cluster"
// @Success 200 {object} Response
// @Failure 400 {object} Response
// @Failure 401 {object} Response
// @Failure 500 {object} Response
// @Router /unjoinManagedCluster [delete]
func (ctr *Controller) UnjoinManagedCluster(c *gin.Context) {
	UnjoinManagedCluster(c)
}

// GetToken retrieves the HUB token with the OCM context
// @Summary Get HUB token with OCM context
// @Security BearerAuth
// @Schemes
// @Description Retrieves the HUB token with OCM context.
// @Tags ocm
// @Accept json
// @Produce json
// @Success 200 {object} Response
// @Failure 500 {object} Response
// @Router /getHubToken [get]
func (ctr *Controller) GetToken(c *gin.Context) {
	log.Info().Msg("Get Token init")

	clientset, _ := k8s.SetupConfig()
	token, err := GetBootstrapTokenFromSA(context.Background(), clientset)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":   "error",
			"response": fmt.Sprintf("Error creating clientset: %v", err),
		})
		return
	}

	c.IndentedJSON(http.StatusOK, gin.H{"token": token, "status": "ok"})
}

// GetInfoHubCluster retrieves information about the HUB cluster
// @Summary Get info from the HUB cluster
// @Security BearerAuth
// @Schemes
// @Description Get info from the HUB about network, OCM, interfaces
// @Tags ocm
// @Accept json
// @Produce json
// @Param cluster_name body string true "Cluster Name"
// @Success 200 {string} string "ok"
// @Router /getInfoHub [get]
func (ctr *Controller) GetInfoHubCluster(c *gin.Context) {
	log.Info().Msg("GetInfoHubCluster init")

	c.IndentedJSON(http.StatusOK, gin.H{"func": "GetInfoHubCluster", "status": "ok"})
}

/************************************************************
						Workloads functions
************************************************************/

// DeployWorkload deploys a workload
// @Summary Deploy a workload
// @Security BearerAuth
// @Schemes
// @Description Integration with Intent Based API to deploy a workload
// @Tags workloads (Intent-API)
// @Accept json
// @Produce json
// @Param helm_chart body string true "Helm Chart"
// @Success 200 {string} string "ok"
// @Router /deployWorkload [post]
func (ctr *Controller) DeployWorkload(c *gin.Context) {
	log.Info().Msg("DeployWorkload init")

	c.IndentedJSON(http.StatusOK, gin.H{"func": "DeployWorkload", "status": "ok"})
}

// UpdateWorkloadStatus updates the status of a workload
// @Summary Update a workload status
// @Security BearerAuth
// @Schemes
// @Description This request will update the status of the workload for the Intent API
// @Tags workloads (Intent-API)
// @Accept json
// @Produce json
// @Param helm_chart body string true "Helm Chart"
// @Success 200 {string} string "ok"
// @Router /updateWorkloadStatus [put]
func (ctr *Controller) UpdateWorkloadStatus(c *gin.Context) {
	log.Info().Msg("UpdateWorkloadStatus init")

	c.IndentedJSON(http.StatusOK, gin.H{"func": "UpdateWorkloadStatus", "status": "ok"})
}

// AddClusterToArgoCD adds an OCM cluster to ArgoCD
// @Summary Add an OCM Cluster to ArgoCD
// @Security BearerAuth
// @Schemes
// @Description Using the ArgoCD API, the HUB cluster is adding an existing cluster inside OCM to the ArgoCD list of clusters.
// @Tags workloads (Intent-API)
// @Accept json
// @Produce json
// @Param argocd_token body string true "ArgoCD Token"
// @Success 200 {string} string "ok"
// @Router /addClusterArgo [put]
func (ctr *Controller) AddClusterToArgoCD(c *gin.Context) {
	log.Info().Msg("AddClusterToArgoCD init")

	c.IndentedJSON(http.StatusOK, gin.H{"func": "AddClusterToArgoCD", "status": "ok"})
}
