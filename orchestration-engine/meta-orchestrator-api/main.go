/*******************************************************************************
 * Copyright 2023 Bull SAS
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/
package main

import (
	_ "mo_api/docs"
	"mo_api/router"
	"net/http"

	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
)

// @host 	mo.nemo.onelab.eu
// @BasePath /
// @title 	MetaOrchestrator API
// @version	1.0.4-beta
// @description A MetaOrchestrator service API in Go using Gin framework

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html

// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
// @BasePath  /
func main() {
	log.Info().Msg("Started Server!")
	// Cargar variables de entorno desde el archivo .env
	if err := godotenv.Load(); err != nil {
		panic("Error loading .env file")
	}
	routes := router.NewRouter()

	server := &http.Server{
		Addr:    ":8080",
		Handler: routes,
	}

	server.ListenAndServe()
}
