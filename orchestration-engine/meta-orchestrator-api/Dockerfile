# Stage 1: Build the application
FROM golang:1.22.3-alpine AS builder

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy go.mod and go.sum files
COPY go.mod go.sum ./

# Download dependencies
RUN go mod download

# Copy the source code into the container
COPY . .

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o main .

# Stage 2: Create the final image
FROM scratch

# Set the Current Working Directory inside the container
WORKDIR /root/

COPY .env ./

# Copy the pre-built binary from the builder stage
COPY --from=builder /app/main .

# Copy the kubeconfig if needed (adjust path as necessary)
COPY --from=builder /app/kubeconfig /root/.kube/config


# Command to run the executable
CMD ["./main"]