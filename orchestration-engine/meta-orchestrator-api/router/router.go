/*******************************************************************************
 * Copyright 2023 Bull SAS
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/
package router

import (
	"mo_api/controller"
	"mo_api/middleware"
	"net/http"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func NewRouter() *gin.Engine {
	router := gin.Default()

	// Set JWT middleware
	authorized := router.Group("/")
	authorized.Use(middleware.JWTAuth())

	basiccontroller := &controller.Controller{}

	// Add swagger
	router.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	router.GET("", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, "welcome home")
	})

	/************************************************************
						 OCM routes
	 ************************************************************/
	router.POST("/login", basiccontroller.Login)
	authorized.GET("/getHubToken", basiccontroller.GetToken)
	authorized.GET("/getInfoHub", basiccontroller.GetInfoHubCluster)
	authorized.GET("/getInfoManagedCluster", basiccontroller.GetInfoManagedClusters)
	authorized.PUT("/joinManagedCluster", basiccontroller.JoinManagedCluster)
	authorized.DELETE("/unJoinmanagedCluster", basiccontroller.UnjoinManagedCluster)
	/************************************************************
						 Workloads routes
	 ************************************************************/
	authorized.GET("/deployWorkload", basiccontroller.DeployWorkload)
	authorized.PUT("/UpdateWorkloadStatus", basiccontroller.UpdateWorkloadStatus)
	authorized.PUT("/addClusterArgo", basiccontroller.AddClusterToArgoCD)

	/************************************************************
						 Migration routes
	 ************************************************************/

	return router
}
