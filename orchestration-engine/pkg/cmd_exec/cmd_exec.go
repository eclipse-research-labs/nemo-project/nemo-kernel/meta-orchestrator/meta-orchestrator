/*******************************************************************************
 * Copyright 2023 Bull SAS
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package cmd_exec

import (
	"bytes"
	"context"
	"io"
	"os"
	"os/exec"
)

// Runs the commands contained in a slice with the option of direct console output, empties the slice when done
func Run(commands []string) string {
	var stdBuffer bytes.Buffer
	var err error
	ctx := context.Background()
	for _, command := range commands {
		cmd := exec.CommandContext(ctx, "bash", "-c", command)
		mw := io.MultiWriter(os.Stdout, &stdBuffer)
		cmd.Stdout = mw
		cmd.Stderr = mw
		if err != nil {
			panic(err)
		}
		cmd.Run()
	}
	return stdBuffer.String()
}

// Returns the list of existing clusters
func Get_clusters() string {
	ctx := context.Background()
	existing_clusters, err := exec.CommandContext(ctx, "bash", "-c", "kubectl config get-contexts -o name").Output()
	if err != nil {
		panic(err)
	}
	return string(existing_clusters)
}
