/*******************************************************************************
 * Copyright 2023 Bull SAS
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package yaml_generator

import (
	"context"
	"fmt"
	"os"
	"os/exec"
	"time"

	"gopkg.in/yaml.v3"
)

type Inputyaml struct {
	NumberOfClusters int    `yaml:"numberofclusters"`
	MinioURL         string `yaml:"minioURL"`
	Name             string `yaml:"name"`
	Namespace        string `yaml:"namespace"`
	Pathname         string `yaml:"pathname"`
	BackupName       string `yaml:"backupName"`
	SourceCluster    string `yaml:"sourceCluster"`
	TargetCluster    string `yaml:"targetCluster"`
	MigrationAction  string `yaml:"migrationAction"`
	Workload         string `yaml:"workload"`
}

type WorkloadStatus struct {
	Cluster  string `yaml:"cluster"`
	Workload string `yaml:"workload"`
	Status   string `yaml:"status"`
}

type Namespaces struct {
	ApiVersion string `yaml:"apiVersion"`
	Kind       string `yaml:"kind"`
	Metadata   struct {
		Name string `yaml:"name"`
	}
}

type Channel struct {
	ApiVersion string `yaml:"apiVersion"`
	Kind       string `yaml:"kind"`
	Metadata   struct {
		Name      string `yaml:"name"`
		Namespace string `yaml:"namespace"`
	}
	Spec struct {
		Type               string `yaml:"type"`
		Pathname           string `yaml:"pathname"`
		InsecureSkipVerify bool   `yaml:"insecureSkipVerify"`
	}
}

type Clusterbinding struct {
	ApiVersion string `yaml:"apiVersion"`
	Kind       string `yaml:"kind"`
	Metadata   struct {
		Name      string `yaml:"name"`
		Namespace string `yaml:"namespace"`
	}
	Spec struct {
		ClusterSet string `yaml:"clusterSet"`
	}
}

type Placement struct {
	ApiVersion string `yaml:"apiVersion"`
	Kind       string `yaml:"kind"`
	Metadata   struct {
		Name      string `yaml:"name"`
		Namespace string `yaml:"namespace"`
	}
	Spec struct {
		NumberOfClusters int      `yaml:"numberOfClusters"`
		ClusterSets      []string `yaml:"clusterSets"`
	}
}

type Subscription struct {
	ApiVersion string `yaml:"apiVersion"`
	Kind       string `yaml:"kind"`
	Metadata   struct {
		Annotations map[string]string `yaml:"annotations"`
		Name        string            `yaml:"name"`
		Namespace   string            `yaml:"namespace"`
	}
	Spec struct {
		Channel   string `yaml:"channel"`
		Placement struct {
			PlacementRef struct {
				Kind      string `yaml:"kind"`
				Name      string `yaml:"name"`
				Namespace string `yaml:"namespace"`
			} `yaml:"placementRef"`
		}
	}
}

// Runs the commands contained in a slice with the option of direct console output, empties the slice when done
func cmd_exec(commands *[]string, verbose bool) {
	ctx := context.Background()
	for _, command := range *commands {
		cmd := exec.CommandContext(ctx, "bash", "-c", command)
		if verbose {
			cmd.Stdout = os.Stdout
		}
		cmd.Stderr = os.Stderr
		cmd.Run()
		time.Sleep(1 * time.Second)
	}
	*commands = nil
}

func Exec(data []byte) {

	// Create an instance of the input yaml struct
	var input Inputyaml

	// Assign default values
	input.Namespace = "default"

	// Unmarshal the YAML data into the struct
	err := yaml.Unmarshal(data, &input)
	if err != nil {
		panic(err)
	}

	if input.Name == "" || input.Pathname == "" {
		fmt.Println("Name of the subscription and pahtname must be specified")
		os.Exit(0)
	}

	// Create an instance of the Namespaces struct with sample data
	namespace1 := Namespaces{
		ApiVersion: "v1",
		Kind:       "Namespace",
		Metadata: struct {
			Name string `yaml:"name"`
		}{
			Name: "gitops-chn-ns",
		},
	}

	namespace2 := Namespaces{
		ApiVersion: "v1",
		Kind:       "Namespace",
		Metadata: struct {
			Name string `yaml:"name"`
		}{
			Name: input.Namespace,
		},
	}

	// Serialize the namespace struct into YAML format
	data, err = yaml.Marshal(&namespace1)
	if err != nil {
		panic(err)
	}
	// Concatenate the yaml data separated with '---'
	namespacesconcat := append([]byte("---\n"), data...)
	namespacesconcat = append(namespacesconcat, []byte("---\n")...)
	data, err = yaml.Marshal(&namespace2)
	if err != nil {
		panic(err)
	}
	namespacesconcat = append(namespacesconcat, data...)

	// Write the serialized YAML data to a file named "namespaces.yaml"
	err = os.WriteFile("../integration-component/manifest/namespaces.yaml", namespacesconcat, 0644)
	if err != nil {
		panic(err)
	}

	// Create an instance of the Channel struct with sample data
	channel := Channel{
		ApiVersion: "apps.open-cluster-management.io/v1",
		Kind:       "Channel",
		Metadata: struct {
			Name      string `yaml:"name"`
			Namespace string `yaml:"namespace"`
		}{
			Name:      input.Name + "-chn",
			Namespace: "gitops-chn-ns",
		},
		Spec: struct {
			Type               string `yaml:"type"`
			Pathname           string `yaml:"pathname"`
			InsecureSkipVerify bool   `yaml:"insecureSkipVerify"`
		}{
			Type:               "HelmRepo",
			Pathname:           input.Pathname,
			InsecureSkipVerify: true,
		},
	}

	// Serialize the namespace struct into YAML format
	data, err = yaml.Marshal(&channel)
	if err != nil {
		panic(err)
	}

	// Write the serialized YAML data to a file named "channel.yaml"
	err = os.WriteFile("../integration-component/manifest/channel.yaml", data, 0644)

	// Create an instance of the Clusterbinding struct with sample data
	clusterbinding := Clusterbinding{
		ApiVersion: "cluster.open-cluster-management.io/v1beta2",
		Kind:       "ManagedClusterSetBinding",
		Metadata: struct {
			Name      string `yaml:"name"`
			Namespace string `yaml:"namespace"`
		}{
			Name:      "default",
			Namespace: input.Namespace,
		},
		Spec: struct {
			ClusterSet string `yaml:"clusterSet"`
		}{
			ClusterSet: "default",
		},
	}

	// Serialize the namespace struct into YAML format
	data, err = yaml.Marshal(&clusterbinding)
	if err != nil {
		panic(err)
	}

	// Write the serialized YAML data to a file named "clusterbinding.yaml"
	err = os.WriteFile("../integration-component/manifest/clusterbinding.yaml", data, 0644)

	// Create an instance of the Placement struct with sample data
	placement := Placement{
		ApiVersion: "cluster.open-cluster-management.io/v1beta1",
		Kind:       "Placement",
		Metadata: struct {
			Name      string `yaml:"name"`
			Namespace string `yaml:"namespace"`
		}{
			Name:      input.Name + "-plcmt",
			Namespace: input.Namespace,
		},
		Spec: struct {
			NumberOfClusters int      `yaml:"numberOfClusters"`
			ClusterSets      []string `yaml:"clusterSets"`
		}{
			NumberOfClusters: 100,
			ClusterSets:      []string{"default"},
		},
	}

	// Serialize the namespace struct into YAML format
	data, err = yaml.Marshal(&placement)
	if err != nil {
		panic(err)
	}

	// Write the serialized YAML data to a file named "clusterbinding.yaml"
	err = os.WriteFile("../integration-component/manifest/placement.yaml", data, 0644)

	// Create an instance of the Subscription struct with sample data
	subscription := Subscription{
		ApiVersion: "apps.open-cluster-management.io/v1",
		Kind:       "Subscription",
		Metadata: struct {
			Annotations map[string]string `yaml:"annotations"`
			Name        string            `yaml:"name"`
			Namespace   string            `yaml:"namespace"`
		}{
			/*Annotations: map[string]string{
				 "apps.open-cluster-management.io/git-branch": "develop",
				 "apps.open-cluster-management.io/git-path":   "prometheus-agent-mode/prometheus-agent-apache",
			 },*/
			Name:      input.Name + "-sub",
			Namespace: input.Namespace,
		},
		Spec: struct {
			Channel   string `yaml:"channel"`
			Placement struct {
				PlacementRef struct {
					Kind      string `yaml:"kind"`
					Name      string `yaml:"name"`
					Namespace string `yaml:"namespace"`
				} `yaml:"placementRef"`
			}
		}{
			Channel: "gitops-chn-ns/" + input.Name + "-chn",
			Placement: struct {
				PlacementRef struct {
					Kind      string `yaml:"kind"`
					Name      string `yaml:"name"`
					Namespace string `yaml:"namespace"`
				} `yaml:"placementRef"`
			}{
				PlacementRef: struct {
					Kind      string `yaml:"kind"`
					Name      string `yaml:"name"`
					Namespace string `yaml:"namespace"`
				}{
					Kind:      "Placement",
					Name:      input.Name + "-plcmt",
					Namespace: input.Namespace,
				},
			},
		},
	}

	// Serialize the namespace struct into YAML format
	data, err = yaml.Marshal(&subscription)
	if err != nil {
		panic(err)
	}

	// Write the serialized YAML data to a file named "clusterbinding.yaml"
	err = os.WriteFile("../integration-component/manifest/subscription.yaml", data, 0644)

	if err != nil {
		panic(err)
	}
	fmt.Println("Data written to yamls")
}
