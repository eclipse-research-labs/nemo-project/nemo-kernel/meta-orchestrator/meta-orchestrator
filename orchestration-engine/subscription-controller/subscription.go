/*******************************************************************************
 * Copyright 2023 Bull SAS
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package main

import (
	"fmt"
	"log"
	"net/url"
	"orchestration-engine/pkg/cmd_exec"
	"orchestration-engine/pkg/yaml_generator"
	"os"

	"github.com/streadway/amqp"
)

func main() {

	// RabbitMQ connection URL
	username := os.Getenv("RABBITMQ_USERNAME")
	password := os.Getenv("RABBITMQ_PASSWORD")
	rabbitMQServerIP := os.Getenv("RABBITMQ_SERVER_IP")
	rabbitMQPort := os.Getenv("RABBITMQ_PORT")
	connURL := fmt.Sprintf("amqp://%s:%s@%s:%s/", url.QueryEscape(username), url.QueryEscape(password), rabbitMQServerIP, rabbitMQPort)

	// Connect to RabbitMQ
	conn, err := amqp.Dial(connURL)
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %v", err)
	}
	defer conn.Close()

	// Create a channel
	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %v", err)
	}
	defer ch.Close()

	// Declare a queue to consume messages from
	queueName := "mo-subscription"
	_, err = ch.QueueDeclare(
		queueName, // name
		true,      // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		log.Fatalf("Failed to declare a queue: %v", err)
	}

	// Consume messages from the queue
	msgs, err := ch.Consume(
		queueName, // queue
		"",        // consumer
		true,      // auto-ack
		false,     // exclusive
		false,     // no-local
		false,     // no-wait
		nil,       // args
	)
	if err != nil {
		log.Fatalf("Failed to register a consumer: %v", err)
	}

	fmt.Println("Awaiting signal...")

	// Infinite loop to continuously read messages
	for msg := range msgs {
		fmt.Printf("Received a message: %s\n", msg.Body)
		yaml_generator.Exec(msg.Body)
		commands := []string{"kubectl apply -f ./manifest/namespaces.yaml --context kind-hub", "kubectl apply -f ./manifest/ --context kind-hub"}
		cmd_exec.Run(commands)

		fmt.Println("\nMigration completed sucessfully!")
	}

}
