/*******************************************************************************
 * Copyright 2023 Bull SAS
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package main

import (
	"flag"
	"fmt"
	"log"
	"net/url"
	"orchestration-engine/pkg/cmd_exec"
	"os"

	"github.com/streadway/amqp"
)

type Connection struct {
	Address string
	Token   string
}

func main() {

	// Define the flags
	hubapiserver := flag.String("hubapiserver", "", "<IP:port of the hub-apiserver>")
	clustername := flag.String("clustername", "", "<Name of the managedcluster>")
	clusterURL := flag.String("clusterURL", "", "<IP:port of the cluster-apiserver>")
	kind := flag.Bool("kind", false, "<Bool> True if using kind (Optional)")

	// Set the usage message
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage:\n")
		flag.PrintDefaults()
	}

	// Parse the command-line flags
	flag.Parse()

	// Check if any of the flags are empty
	if *hubapiserver == "" || *clustername == "" || *clusterURL == "" {
		fmt.Println("Error: All flags are mandatory")
		flag.Usage()
		os.Exit(1)
	}

	// RabbitMQ connection URL
	username := os.Getenv("RABBITMQ_USERNAME")
	password := os.Getenv("RABBITMQ_PASSWORD")
	rabbitMQServerIP := os.Getenv("RABBITMQ_SERVER_IP")
	rabbitMQPort := os.Getenv("RABBITMQ_PORT")
	connURL := fmt.Sprintf("amqp://%s:%s@%s:%s/", url.QueryEscape(username), url.QueryEscape(password), rabbitMQServerIP, rabbitMQPort)

	// Create a new RabbitMQ connection.
	connectRabbitMQ, err := amqp.Dial(connURL)
	if err != nil {
		panic(err)
	}
	defer connectRabbitMQ.Close()

	// Open a channel
	channelRabbitMQ, err := connectRabbitMQ.Channel()
	if err != nil {
		panic(err)
	}
	defer channelRabbitMQ.Close()

	// Join to the hub cluster
	messages, err := channelRabbitMQ.Consume("mo-join", "", false, true, false, false, nil)
	msg := <-messages
	commands := []string{"clusteradm join --hub-token " + string(msg.Body) + " --hub-apiserver " + *hubapiserver + " --wait --cluster-name " + *clustername}
	if *kind {
		commands = []string{"clusteradm join --hub-token " + string(msg.Body) + " --hub-apiserver " + *hubapiserver + " --wait --force-internal-endpoint-lookup --cluster-name " + *clustername}
	}
	cmd_exec.Run(commands)

	// Create a new queue for migration
	queueName := "ibmc-" + *clustername
	_, err = channelRabbitMQ.QueueDeclare(
		queueName, // name
		true,      // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		log.Fatalf("Failed to declare a queue: %v", err)
	}

	// Bind the queue to the ibmc exchange and add routing key
	err = channelRabbitMQ.QueueBind(
		queueName,    // queue name
		*clustername, // routing key
		"ibmc",       // exchange name
		false,        // no-wait
		nil,          // arguments
	)
	if err != nil {
		log.Fatalf("Failed to bind the queue to the exchange: %v", err)
	}

	log.Printf("Queue '%s' created successfully", queueName)

}
