/*******************************************************************************
 * Copyright 2023 Bull SAS
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package main

import (
	"context"
	"fmt"
	"log"
	"net/url"
	"os"

	"orchestration-engine/pkg/k8s"

	"github.com/streadway/amqp"
	authv1 "k8s.io/api/authentication/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/utils/ptr"
	"open-cluster-management.io/clusteradm/pkg/config"
)

func GetBootstrapTokenFromSA(ctx context.Context, kubeClient kubernetes.Interface) (string, error) {
	tr, err := kubeClient.CoreV1().
		ServiceAccounts(config.OpenClusterManagementNamespace).
		CreateToken(ctx, config.BootstrapSAName, &authv1.TokenRequest{
			Spec: authv1.TokenRequestSpec{
				// token expired in 1 hour
				ExpirationSeconds: ptr.To[int64](3600),
			},
		}, metav1.CreateOptions{})
	if err != nil {
		return "", fmt.Errorf("failed to get token from sa %s/%s: %v", config.OpenClusterManagementNamespace, config.BootstrapSAName, err)
	}
	return tr.Status.Token, nil
}
func main() {

	//Setup k8s config
	clientset, _ := k8s.SetupConfig()
	//Get OCM token
	token, err := GetBootstrapTokenFromSA(context.Background(), clientset)
	if err != nil {
		fmt.Printf("Error creating clientset: %v\n", err)
		os.Exit(1)
	}

	// RabbitMQ connection URL
	username := os.Getenv("RABBITMQ_USERNAME")
	password := os.Getenv("RABBITMQ_PASSWORD")
	rabbitMQServerIP := os.Getenv("RABBITMQ_SERVER_IP")
	rabbitMQPort := os.Getenv("RABBITMQ_PORT")
	connURL := fmt.Sprintf("amqp://%s:%s@%s:%s/", url.QueryEscape(username), url.QueryEscape(password), rabbitMQServerIP, rabbitMQPort)

	// Create a new RabbitMQ connection
	connectRabbitMQ, err := amqp.Dial(connURL)
	if err != nil {
		panic(err)
	}

	// Open a channel
	channelRabbitMQ, err := connectRabbitMQ.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %v", err)
	}
	defer channelRabbitMQ.Close()

	//Purge the queue before publishing a new token
	_, err = channelRabbitMQ.QueuePurge("mo-join", false)
	if err != nil {
		log.Fatal(err)
	}

	// Publish a message
	err = channelRabbitMQ.Publish(
		"mo",           // exchange
		"join-cluster", // routing key
		false,          // mandatory
		false,          // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(token),
		},
	)
	if err != nil {
		log.Fatalf("Failed to publish the token: %v", err)
	}
	fmt.Println("Token has been published succesfully")
}
