/*******************************************************************************
 * Copyright 2023 Bull SAS
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package main

import (
	"context"
	"fmt"
	"orchestration-engine/pkg/k8s"

	certificatesv1 "k8s.io/api/certificates/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Define the structure of the JSON data
type Item struct {
	APIVersion string `json:"apiVersion"`
	Kind       string `json:"kind"`
	Metadata   struct {
		Name string `json:"name"`
	} `json:"metadata"`
	Status map[string]interface{} `json:"status"`
}

// Helper function to check if a CSR is pending
func isPending(csr certificatesv1.CertificateSigningRequest) bool {
	for _, condition := range csr.Status.Conditions {
		if condition.Type == certificatesv1.CertificateApproved || condition.Type == certificatesv1.CertificateDenied {
			return false
		}
	}
	return true
}

func main() {

	//Setup k8s config
	clientset, _ := k8s.SetupConfig()

	// Get CertificateSigningRequests
	csrList, err := clientset.CertificatesV1().CertificateSigningRequests().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		fmt.Printf("Error getting CertificateSigningRequests: %v\n", err)
		return
	}

	// Check if there are pending CertificateSigningRequests
	hasPendingCSRs := false
	for _, csr := range csrList.Items {
		if isPending(csr) {
			hasPendingCSRs = true
			break
		}
	}

	// If no pending CSRs found, inform the user and exit
	if !hasPendingCSRs {
		fmt.Println("No pending CertificateSigningRequests to approve")
		return
	}

	//Approve the pending CertificateSigningRequests
	fmt.Println("Approving pending CertificateSigningRequests:")
	for _, csr := range csrList.Items {
		if isPending(csr) {
			// Approve the CSR by updating its status
			csr.Status.Conditions = append(csr.Status.Conditions, certificatesv1.CertificateSigningRequestCondition{
				Type:    certificatesv1.CertificateApproved,
				Status:  "True",
				Reason:  "AutoApproved",
				Message: "This CSR was automatically approved.",
			})
			_, err := clientset.CertificatesV1().CertificateSigningRequests().UpdateApproval(context.Background(), csr.Name, &csr, metav1.UpdateOptions{})
			if err != nil {
				fmt.Printf("Error approving CSR %s: %v\n", csr.Name, err)
			} else {
				fmt.Printf("- Approved CSR %s\n", csr.Name)
			}
		}
	}
}
