# NEMO Kernel Space

- [NEMO Kernel Space](#nemo-kernel-space)
  - [Introduction](#introduction)
  - [NEMO meta-Orchestrator](#nemo-meta-orchestrator)
    - [Orchestration Engine](#orchestration-engine)
      - [Prerequisites](#prerequisites)
    - [Resource Manager](#resource-manager)
    - [Decision Engine](#decision-engine)
    - [Analytics Engine](#analytics-engine)
    - [Integration Component](#integration-component)
  - [NEMO Intent-Based Migration Controller](#nemo-intent-based-migration-controller)

## Introduction



## NEMO meta-Orchestrator

![meta-Orchestrator](img/metaOrchestrator.svg)

The meta-orchestrator component is designed as a highly advanced and intelligent open-source software system. Its primary goal is to enable the decentralization and distribution of computing workflows across the IoT to Edge to Cloud Continuum. By acting as a central orchestrator, it manages the coordination and execution of complex distributed systems while addressing the challenges posed by their increasing complexity and heterogeneity.
The meta-orchestrator provides a comprehensive and holistic approach to orchestration by considering various aspects of distributed computing workflows. It conducts an in-depth investigation of the structure and application programming interfaces (APIs) of various micro-schedulers and local orchestrators. This analysis allows the meta-orchestrator to seamlessly integrate and coordinate with different components within the distributed system architecture.

The intelligence capabilities of the meta-orchestrator are at the core of its decision-making process. It considers crucial parameters such as migration time, downtime, and overhead time when orchestrating computing workflows. By evaluating these parameters, the meta-orchestrator ensures that workflows are orchestrated in a manner that minimizes disruption and maximizes efficiency.

The meta-orchestrator acts as a central hub for managing distributed systems across the IoT to Edge to Cloud Continuum. It provides a high-level view of the system, allowing for efficient coordination and resource management. By leveraging the capabilities of micro-schedulers and local orchestrators, the meta-orchestrator ensures that computing workflows are distributed effectively and executed on the most suitable resources available within the continuum.

The meta-orchestrator's intelligent decision-making capabilities not only optimize resource utilization but also contribute to the scalability and adaptability of the distributed system. It can dynamically adjust the allocation of resources based on changing conditions and requirements. This adaptability is especially valuable in scenarios where the system experiences fluctuations in workload, availability of resources, or environmental conditions.

Moreover, the meta-orchestrator fosters interoperability and compatibility across different components and systems within the distributed architecture. It provides standardized interface that allow for seamless integration and communication with other components. This interoperability ensures that the distributed system operates cohesively and efficiently, even when composed of heterogeneous and diverse components


- The [**orchestration engine**](#orchestration-engine) serves as a central component within NEMO, abstracting the underlying complexities of diverse orchestration tools and frameworks. It provides a unified integration point for different platforms, allowing seamless interoperability and coordination among heterogeneous systems. The orchestration engine enables the meta-orchestrator to manage and control the distributed computing workflow efficiently.
- The [**resource manager**](#resource-manager) component is responsible for managing the complete lifecycle of resources. It handles tasks such as provisioning, scaling, monitoring, and deprovisioning of resources in a distributed environment. By effectively managing the allocation and deallocation of resources, the resource manager ensures optimal utilization and availability for the computing workflows orchestrated by NEMO.
- The [**decision engine**](#decision-engine) plays a vital role in providing intelligent decision-making capabilities. It incorporates policies for policy enforcement, optimizing costs, and determining the placement of workloads. By considering various factors such as workload characteristics, resource availability, and performance metrics, the decision engine ensures efficient orchestration of computing workflows while adhering to specified policies and minimizing operational costs.
- The [**analytics engine**](#analytics-engine) component focuses on collecting and analyzing data related to resource utilization, performance, and other relevant metrics. By leveraging this data, the analytics engine generates insights and recommendations to optimize the overall orchestration process. These insights aid in identifying potential bottlenecks, improving resource allocation strategies, and enhancing the overall performance of the distributed computing environment.
- The [**integration component**](#integration-component) provides connectors and APIs to facilitate seamless integration with different orchestration tools and frameworks. By establishing compatibility and interoperability, the integration component enables the meta-orchestrator to leverage existing infrastructure and frameworks efficiently. This component simplifies the adoption and deployment of NEMO within diverse computing ecosystems.

### Orchestration Engine

#### Prerequisites

List of software needed to have the Orchestration Engine working:

- [Go](https://go.dev/)
- [Docker](https://www.docker.com/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/)
- [kustomize](https://kubectl.docs.kubernetes.io/installation/kustomize/)
- [Clusteradm CLI](https://open-cluster-management.io/getting-started/installation/start-the-control-plane/#install-clusteradm-cli-tool)

A hub cluster with OCM installed is also needed. The installation steps can be found in: https://open-cluster-management.io/getting-started/installation/start-the-control-plane/

#### Getting Started

First, the hub token needs to be published to rabbitmq so that new clusters joining NEMO can retrieve it. In the hub cluster, run the following code:
```sh
go run publish-token.go
```
To begin the onboarding process, from the joining cluster run:
```sh
go run setup-cluster.go
``` 
The following arguments must be specified:
```sh
  -clusterURL <IP:port of the cluster-apiserver>
  -clustername <Name of the managedcluster>
  -hubapiserver <IP:port of the hub-apiserver>
  -kind <Bool> True if using kind (Optional)
``` 
The request for joining OCM must be approved from the hub cluster, in order to do that run:
 ```sh
go run accept-cluster.go
``` 
When the setup is finished, it will be possible to manage the cluster from OCM and also will be added to argocd.

##### Subscription service
For using the subscription service go to the integration-component folder and do:
```sh
go run subscription.go
```
In a different terminal, run subscription.yaml subscribe to the repo indicated in the subscription.yaml:
```sh
go run subscription_publisher.go
```